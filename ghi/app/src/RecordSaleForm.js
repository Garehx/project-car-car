import React, { useEffect, useState } from 'react';

function RecordSaleForm() {
  const [vinData, setVinData] = useState([]);
  const [selectedVin, setVin] = useState('');
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [customers, setCustomers] = useState([]);
  const [selectedCustomer, setSelectedCustomer] = useState('');
  const [salePrice, setSalePrice] = useState('');

  const defaultVin = '';
  const defaultSalesperson = '';
  const defaultCustomer = '';
  const defaultSalePrice = '';

  const fetchData = async () => {
    try {
      const vinUrl = 'http://localhost:8100/api/automobiles/';
      const vinResponse = await fetch(vinUrl);

      if (vinResponse.ok) {
        const data = await vinResponse.json();
        setVinData(data.autos);
      } else {
        console.error('Error fetching automobiles:', vinResponse.statusText);
      }

      const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
      const salespeopleResponse = await fetch(salespeopleUrl);

      if (salespeopleResponse.ok) {
        const data = await salespeopleResponse.json();
        setSalespeople(data.salespeople);
      } else {
        console.error('Error fetching salespeople:', salespeopleResponse.statusText);
      }

      const customersUrl = 'http://localhost:8090/api/customers/';
      const customersResponse = await fetch(customersUrl);

      if (customersResponse.ok) {
        const data = await customersResponse.json();
        setCustomers(data.customers);
      } else {
        console.error('Error fetching customers:', customersResponse.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const handleVinChange = (e) => {
    setVin(e.target.value);
  };

  const handleSalespersonChange = (e) => {
    setSelectedSalesperson(e.target.value);
  };

  const handleCustomerChange = (e) => {
    setSelectedCustomer(e.target.value);
  };

  const handleSalePriceChange = (e) => {
    setSalePrice(e.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const selectedAutomobile = vinData.find((auto) => auto.vin === selectedVin);

      if (selectedAutomobile) {
        // Update the automobile's sold status
        await updateAutomobileStatus(selectedVin);

        const data = {
          automobile: selectedVin,
          salesperson: selectedSalesperson,
          customer: selectedCustomer,
          price: salePrice,
        };

        const fetchConfig = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        };

        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url, fetchConfig);

        if (response.ok) {

          setVinData((prevVinData) =>
            prevVinData.filter((auto) => auto.vin !== selectedVin)
          );

          setVin(defaultVin);
          setSelectedSalesperson(defaultSalesperson);
          setSelectedCustomer(defaultCustomer);
          setSalePrice(defaultSalePrice);

          console.log('Sale recorded successfully!');
        } else {
          console.error('Failed to record sale:', response.statusText);
        }
      } else {
        console.error('Automobile not found for VIN:', selectedVin);
      }
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };

  const updateAutomobileStatus = async (vin) => {

    try {
      const updateStatusUrl = `http://localhost:8100/api/automobiles/update-status/${vin}`;
      const updateStatusResponse = await fetch(updateStatusUrl, { method: 'PUT' });

      if (!updateStatusResponse.ok) {
        console.error('Failed to update automobile status:', updateStatusResponse.statusText);
      }
    } catch (error) {
      console.error('Error updating automobile status:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a New Sale</h1>

            <form onSubmit={handleSubmit} id="record-sale-form">
              <div className="mb-3">
                <label htmlFor="vin">Automobile VIN:</label>
                <select
                  value={selectedVin}
                  onChange={handleVinChange}
                  required
                  id="vin"
                  name="vin"
                  className="form-select"
                >
                  <option value="" disabled>
                    Select an Automobile VIN
                  </option>
                  {vinData.map((vin) => (
                    <option key={vin.id} value={vin.vin}>
                      {vin.vin}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="salesperson">Salesperson:</label>
                <select
                  value={selectedSalesperson}
                  onChange={handleSalespersonChange}
                  required
                  id="salesperson"
                  name="salesperson"
                  className="form-select"
                >
                  <option value="" disabled>
                    Select a Salesperson
                  </option>
                  {salespeople.map((person) => (
                    <option key={person.id} value={person.id}>
                      {person.first_name + person.last_name}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="customer">Customer:</label>
                <select
                  value={selectedCustomer}
                  onChange={handleCustomerChange}
                  required
                  id="customer"
                  name="customer"
                  className="form-select"
                >
                  <option value="" disabled>
                    Select a Customer
                  </option>
                  {customers.map((customer) => (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name + customer.last_name}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="salePrice">Sale Price:</label>
                <input
                  onChange={handleSalePriceChange}
                  placeholder="Enter Sale Price"
                  required
                  type="number"
                  id="salePrice"
                  name="salePrice"
                  className="form-control"
                  value={salePrice}
                />
              </div>

              <button className="btn btn-primary">Record Sale</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RecordSaleForm;
