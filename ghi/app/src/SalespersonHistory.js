import React, { useEffect, useState } from 'react';

function SalesHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');

  const fetchSales = async (salespersonId) => {
    const url = salespersonId
      ? `http://localhost:8090/api/sales/?salesperson=${salespersonId}`
      : 'http://localhost:8090/api/sales/';

    try {
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.error('Error fetching sales data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const fetchSalespeople = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error('Error fetching salespeople:', response.statusText);
    }
  };

  useEffect(() => {
    fetchSales();
    fetchSalespeople();
  }, []);

  const handleSalespersonChange = (e) => {
    setSelectedSalesperson(e.target.value);
    fetchSales(e.target.value);
  };

  return (
    <div>
      <h1>Sales History</h1>
      <div className="mb-3">
        <label htmlFor="salespersonDropdown">Select Salesperson:</label>
        <select
          value={selectedSalesperson}
          onChange={handleSalespersonChange}
          id="salespersonDropdown"
          className="form-select"
        >
          <option value="">All Salespeople</option>
          {salespeople.map((person) => (
            <option key={person.id} value={person.id}>
              {`${person.first_name} ${person.last_name}`}
            </option>
          ))}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
              <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistory;
