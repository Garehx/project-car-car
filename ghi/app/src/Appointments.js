import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function AppointmentList() {
  const [search, setSearch] = useState('')
  const [appointments, setAppointments] = useState([])
  const getData = async () => {
  const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  const FinishAppointment = async (event) => {
  const appointmentID = (event.target.id);
  const url = `http://localhost:8080/api/appointments/${appointmentID}/finish/`
  const response = await fetch(url, {method: 'PUT'})
        .then((response) => {
            if(response.ok){
                getData()
            }
        })
    }

  const CancelAppointment = async (event) => {
  const appointmentID = (event.target.id);
  const url = `http://localhost:8080/api/appointments/${appointmentID}/cancel/`
  const response = await fetch(url, {method: 'PUT'})
        .then((response) => {
            if(response.ok){

                getData()
  }})
    }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Reason</th>
          <th>Status</th>
          <th>Technician</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map(apps => {
          if (apps.status === "Created")
          return (
            <tr key={apps.id}>
              <td>{ apps.vin }</td>
              <td>{ apps.customer }</td>
              <td>{ apps.date_time }</td>
              <td>{ apps.date_time }</td>
              <td>{ apps.reason }</td>
              <td>{ apps.status }</td>
              <td>{ apps.technician.first_name + " " + apps.technician.last_name }</td>
              <td><NavLink to="#" className="btn btn-primary" onClick={FinishAppointment} id={apps.id}>Finish Appointment</NavLink></td>
              <td><NavLink to="#" className="btn btn-primary" onClick={CancelAppointment} id={apps.id}>Cancel Appointment</NavLink></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;