import React, { useEffect, useState } from 'react';

function AllSales() {
  const [sales, setSales] = useState([]);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/sales/');

      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.error('Error fetching sales data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson ID</th>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.salesperson.id}</td>
              <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
              <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AllSales;
