import React, { useEffect, useState } from 'react';


function TechnicianForm(){

    const [first_name, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const [last_name, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const [employee_id, setEmployeeID] = useState('');
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;


        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const techUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeID('');
          }
      }


      useEffect(() => {
      }, []);

return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Technician!</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleFirstNameChange} placeholder="First Name" required type="text" id="first_name" name="first_name" className="form-control" value={first_name} />
              <label htmlFor="picture_url">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleLastNameChange} placeholder="Last Name" required type="text" id="last_name" name="last_name" className="form-control" value={last_name} />
              <label htmlFor="picture_url">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmployeeIDChange} placeholder="Employee ID" required type="text" id="employee_id" name="employee_id" className="form-control" value={employee_id} />
              <label htmlFor="picture_url">Employee Id</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
)
}

export default TechnicianForm;
