import React, { useEffect, useState } from 'react';


function AppointmentForm(){

    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [date_time, setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }


    const [technician, setTechnician] = useState('');
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const [reason, setReason] = useState('');
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const [technicians, setTechnicians] = useState([]);
    let [status, setStatus] = useState('');
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.vin = vin;
        data.customer = customer;
        data.date_time = date_time;
        data.technician = technician;
        data.reason = reason;
        data.status = 'Created';

        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const modelUrl = 'http://localhost:8080/api/appointments/';
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setVin('');
            setCustomer('');
            setDateTime('');
            setTechnician('');
            setReason('');
            setStatus('Created');
          }
      }

    const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);

        }
      }

      useEffect(() => {
        fetchData();
      }, []);

return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an Appointment!
          </h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} placeholder="Vin" required type="text" id="vin" name="vin" className="form-control" value={vin} />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCustomerChange} placeholder="Customer's Name" required type="text" id="customer" name="customer" className="form-control" value={customer} />
              <label htmlFor="customer">Customer's Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleDateTimeChange} placeholder="Date & Time" required type="datetime-local" id="date" name="date" className="form-control" value={date_time} />
              <label htmlFor="datetime">Date & Time</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleReasonChange} placeholder="Reason for visit" required type="text" id="reason" name="reason" className="form-control" value={reason} />
              <label htmlFor="reason">Reason for visit</label>
            </div>
            <div className="mb-3">
              <select value={technician} onChange={handleTechnicianChange} required id="technician" name="technician" className="form-select" >
                <option value="">Choose a technician</option>
                    {technicians.map(techs => {
                        return (
                          <option key={techs.id} value={techs.id}>
                          {techs.first_name}
                          </option>
                        );
                        })}

              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
)
}

export default AppointmentForm;
