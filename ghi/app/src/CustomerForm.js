import React, { useEffect, useState } from 'react';

function CustomerForm() {
  const [first_name, setFirstName] = useState('');
  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const [last_name, setLastName] = useState('');
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const [address, setAddress] = useState('');
  const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
  };

  const [phone_number, setPhoneNumber] = useState('');
  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  };

  const [custumer, setCustomer] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      first_name: first_name,
      last_name: last_name,
      address: address,
      phone_number: phone_number,
    };

    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const salespersonUrl = 'http://localhost:8090/api/customers/';
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      setFirstName('');
      setLastName('');
      setAddress('');
      setPhoneNumber('');
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomer(data.customers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Customer!</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFirstNameChange}
                  placeholder="First Name"
                  required
                  type="text"
                  id="first_name"
                  name="first_name"
                  className="form-control"
                  value={first_name}
                />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleLastNameChange}
                  placeholder="Last Name"
                  required
                  type="text"
                  id="last_name"
                  name="last_name"
                  className="form-control"
                  value={last_name}
                />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleAddressChange}
                  placeholder="Address"
                  required
                  type="text"
                  id="address"
                  name="address"
                  className="form-control"
                  value={address}
                />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handlePhoneNumberChange}
                  placeholder="Phone Number"
                  required
                  type="text"
                  id="phone_number"
                  name="phone_number"
                  className="form-control"
                  value={phone_number}
                />
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
