## sales/sales_rest/views.py

from smtplib import SMTPResponseException
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet
from datetime import datetime, date
from django.urls import NoReverseMatch
from json import JSONEncoder
from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
from .models import Salesperson, Customer, Sale, AutomobileVO

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime, date)):
            return o.isoformat()
        else:
            return super().default(o)

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ['first_name', 'last_name', 'employee_id', "id"]

    def get_extra_data(self, o):
        return {}

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['first_name', 'last_name', 'address', 'phone_number', "id"]

    def get_extra_data(self, o):
        return {}

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ['automobile', 'salesperson', 'customer', 'price', "id"]

    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin}

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin', 'sold', ]

    def get_extra_data(self, o):
        return {}

@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = list(Salesperson.objects.values())
        return JsonResponse({'salespeople': salespeople}, safe=False)
        
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)

            return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"error": f"Could not create the salesperson: {str(e)}"}, status=400, safe=False)

@require_http_methods(["DELETE"])
def api_salesperson(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(pk=id)
            salesperson.delete()
            return JsonResponse({"message": "Salesperson deleted successfully!"}, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"error": "Salesperson does not exist"}, status=404, safe=False)

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({'customers': customers}, encoder=CustomerEncoder, safe=False)
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"error": f"Could not create the customer: {str(e)}"}, status=400, safe=False)

@require_http_methods(["DELETE"])
def api_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(pk=id)
            customer.delete()
            return JsonResponse({"message": "Customer deleted successfully!"}, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"error": "Customer does not exist"}, status=404, safe=False)

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleEncoder)

    content = json.loads(request.body)
    automobile_data = content.get("automobile")
    salesperson = Salesperson.objects.get(id=content["salesperson"])
    customer = Customer.objects.get(id=content["customer"])
    vin = automobile_data.get("vin")

    try:
        automobile = AutomobileVO.objects.get(vin=vin)
    except ObjectDoesNotExist:
        raise ValueError(f"Automobile with VIN {vin} does not exist.")

    sales = Sale.objects.create(
        automobile=automobile,
        salesperson=salesperson,
        customer=customer,
        price=content["price"],
    )

    return JsonResponse({"message": "Sale created successfully!"}, safe=False)

@require_http_methods(["DELETE"])
def api_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(pk=id)
            sale.delete()
            return JsonResponse({"message": "Sale deleted successfully!"})
        except Sale.DoesNotExist:
            return JsonResponse({"error": "Sale does not exist"}, status=404, safe=False)
